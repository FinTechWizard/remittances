from decimal import Decimal
from datetime import datetime

class NettingOrder(object):
    """
    Each transactions are divided into two orders - incoming and outgoing.
    Orders are sent between the nodes. Orders are netted against each other.
    
    Netting allocates orignal amount to amount netted so the original principal is not sent to the
    original destination but to destinations optimized by netting.

    Status shows whether order has been entirely netted.
    
    Variable delta describes how much money from the original amount
    has been already netted.
    """
    def __init__(self, order_type, sender_address, transaction_amount):
        self.type = order_type    # if type reveive address is a delivery address, if type is receive then address is receive address
        self.sender_address = sender_address
        self.amount = transaction_amount
        self.created_at = datetime.now()
        self.status = 'PENDING' # PENDING or NETTED
        self.delivery_addresses = []
        self.delta = self.amount

    def __str__(self):
        return '{0} {1} {2}'.format(self.type, self.sender_address, self.amount)

class Node(object):
    """
    Object that receives incoming orders and registers outgoing orders
    Orders are matched against each other and their delivery addresses
    are overwritten. 

    Originally each transaction may have only one destination
    address. Netting optimizes this procedure and may assign multiple 
    destination addresses to optimize the cost of transfer.
    """

    def __init__(self, currency):
        self.currency = currency
        self.outgoing_orders = []
        self.incoming_orders = []
        self.total_outgoing  = Decimal(0)
        self.total_incoming = Decimal(0)

    def net_orders(self):
        # Matching engine algortihm - usually proprietry - will use sth easy. Will talk about them later
        for outgoing_order in self.outgoing_orders:
            # print outgoing_order
            i = 0
            for incoming_order in self.incoming_orders:
                #if outgoing_order.delta >= incoming_order.amount and incoming_order.status is not 'NETTED':
                if outgoing_order.status is not 'NETTED' and incoming_order.status is not 'NETTED':
                    # print '----------'
                    # print '{0} |--| {1}'.format(outgoing_order, incoming_order)
                    # print '\nDELTAS before netting {0}/{1} & {2}/{3}'.format(outgoing_order.delta, outgoing_order.amount, incoming_order.delta, incoming_order.amount)        
                    if outgoing_order.delta > incoming_order.delta:
                        outgoing_order.delta -= incoming_order.delta
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:incoming_order.delta})
                        incoming_order.delta = 0
                        incoming_order.status = 'NETTED'

                    elif outgoing_order.delta < incoming_order.delta:
                        incoming_order.delta -= outgoing_order.delta
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:outgoing_order.delta})
                        outgoing_order.delta = 0
                        outgoing_order.status = 'NETTED'

                    elif outgoing_order.delta == incoming_order.delta:
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:outgoing_order.delta})
                        incoming_order.delta = 0
                        incoming_order.status = 'NETTED'
                        outgoing_order.delta = 0
                        outgoing_order.status = 'NETTED'
                    # print 'DELTAS after netting {0}/{1} & {2}/{3}\n'.format(outgoing_order.delta, outgoing_order.amount, incoming_order.delta, incoming_order.amount) 
                i += 1

    def execute(self):
        for outgoing_order in self.outgoing_orders:
            for transfer in outgoing_order.delivery_addresses:
                print 'from {0} transfer {1} {2} to {3}'.format(outgoing_order.sender_address,
                                                                transfer.values()[0],
                                                                self.currency,
                                                                transfer.keys()[0])
        print 'From {0} liquidity pool, transfer {1} {2} to {3}'.format(self.currency, self.exposure, self.currency, self.incoming_orders[-1].sender_address)

    def calculate_totals(self):
        for outgoing_order in self.outgoing_orders:
            self.total_outgoing += outgoing_order.amount

        for incoming_order in self.incoming_orders:
            self.total_incoming += incoming_order.amount

        self.exposure = self.total_incoming - self.total_outgoing

