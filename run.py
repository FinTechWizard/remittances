from decimal import *
from datetime import datetime

class Transaction(object):
    """
    General information about transaction
    - counterparties information
    - source and destination currencies
    - source and destination amounts
    - FX rate agreed
    """
    def __init__(self, from_account, to_account, src_currency, dest_currency, src_amount, fx_rate):
        self.from_account = from_account
        self.to_account = to_account
        self.src = src_currency
        self.dest = dest_currency
        self.src_amount = Decimal(src_amount)
        self.fx_rate = Decimal(fx_rate)
        self.dest_amount = self.__compute_dest_amount()

    def __compute_dest_amount(self):
        return Decimal(self.src_amount * self.fx_rate)

    def __str__(self):
        return '{0} transfers {1} {2}, {3} receives {4} {5}'.format(self.from_account, self.src_amount, self.src,
                                                                    self.to_account, self.dest_amount, self.dest)

class NettingOrder(object):
    """
    Each transactions are divided into two orders - incoming and outgoing.
    Orders are sent between the nodes. Orders are netted against each other.
    
    Netting allocates orignal amount to delta so the original principal is not sent to the
    original destination but to destinations optimized by netting.

    Status shows whether order has been entirely netted.
    
    Variable delta describes how much money from the original amount
    has been already netted.
    """
    def __init__(self, order_type, sender_address, transaction_amount):
        self.type = order_type    # if type reveive address is a delivery address, if type is receive then address is receive address
        self.sender_address = sender_address
        self.amount = transaction_amount
        self.created_at = datetime.now()
        self.status = 'PENDING' # PENDING or NETTED
        self.delivery_addresses = []
        self.delta = self.amount

    def __str__(self):
        return '{0} {1} {2}'.format(self.type, self.sender_address, self.amount)


class Node(object):
    """
    Object that receives incoming orders and registers outgoing orders
    Orders are matched against each other and their delivery addresses
    are overwritten. 

    Originally each transaction may have only one destination
    address. Netting optimizes this procedure and may assign multiple 
    destination addresses to optimize the cost of transfer.
    """

    def __init__(self, currency):
        self.currency = currency
        self.outgoing_orders = []
        self.incoming_orders = []
        self.total_outgoing  = Decimal(0)
        self.total_incoming = Decimal(0)

    def net_orders(self):
        # Matching engine algortihm - usually proprietry - will use sth easy. Will talk about them later
        for outgoing_order in self.outgoing_orders:
            # print outgoing_order
            i = 0
            for incoming_order in self.incoming_orders:
                #if outgoing_order.delta >= incoming_order.amount and incoming_order.status is not 'NETTED':
                if outgoing_order.status is not 'NETTED' and incoming_order.status is not 'NETTED':
                    # print '----------'
                    # print '{0} |--| {1}'.format(outgoing_order, incoming_order)
                    # print '\nDELTAS before netting {0}/{1} & {2}/{3}'.format(outgoing_order.delta, outgoing_order.amount, incoming_order.delta, incoming_order.amount)        
                    if outgoing_order.delta > incoming_order.delta:
                        outgoing_order.delta -= incoming_order.delta
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:incoming_order.delta})
                        incoming_order.delta = 0
                        incoming_order.status = 'NETTED'

                    elif outgoing_order.delta < incoming_order.delta:
                        incoming_order.delta -= outgoing_order.delta
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:outgoing_order.delta})
                        outgoing_order.delta = 0
                        outgoing_order.status = 'NETTED'

                    elif outgoing_order.delta == incoming_order.delta:
                        outgoing_order.delivery_addresses.append({incoming_order.sender_address:outgoing_order.delta})
                        incoming_order.delta = 0
                        incoming_order.status = 'NETTED'
                        outgoing_order.delta = 0
                        outgoing_order.status = 'NETTED'
                    # print 'DELTAS after netting {0}/{1} & {2}/{3}\n'.format(outgoing_order.delta, outgoing_order.amount, incoming_order.delta, incoming_order.amount) 
                i += 1

    def execute(self):
        for outgoing_order in self.outgoing_orders:
            for transfer in outgoing_order.delivery_addresses:
                print 'from {0} transfer {1} {2} to {3}'.format(outgoing_order.sender_address,
                                                                transfer.values()[0],
                                                                self.currency,
                                                                transfer.keys()[0])
        print 'From {0} liquidity pool, transfer {1} {2} to {3}'.format(self.currency, self.exposure, self.currency, self.incoming_orders[-1].sender_address)

    def calculate_totals(self):
        for outgoing_order in self.outgoing_orders:
            self.total_outgoing += outgoing_order.amount

        for incoming_order in self.incoming_orders:
            self.total_incoming += incoming_order.amount

        self.exposure = self.total_incoming - self.total_outgoing


def main():
    # set floating point precision for decimals
    getcontext().prec = 6 

    # Initialise all the nodes in the system
    netting_engine = {
        'GBP' : Node('GBP'),
        'USD' : Node('USD'),
        'JPY' : Node('JPY')
    }

    # Initialise all prices in the system
    # GBPEUR = Decimal(1.1887)
    GBPUSD = Decimal(1.2)
    USDGBP = Decimal(1 / GBPUSD)

    USDJPY = Decimal(140)
    JPYUSD = Decimal(1 / USDJPY)

    GBPJPY = Decimal(GBPUSD * USDJPY)
    JPYGBP = Decimal(1 / GBPJPY)

    # Registers some transactions between the nodes
    transactions = list()
    transactions.append(Transaction('Max', 'brother Filip', 'USD', 'GBP', 1000, USDGBP))
    transactions.append(Transaction('Parents', 'son Mike', 'GBP', 'USD', 500, GBPUSD))
    transactions.append(Transaction('Grandpa', 'Emily', 'GBP', 'USD', 300, GBPUSD))

    transactions.append(Transaction('Haruko', 'Sam', 'JPY', 'GBP', 1000, JPYGBP))
    transactions.append(Transaction('Ed', 'Mitsu', 'GBP', 'JPY', 40, GBPJPY))   

    transactions.append(Transaction('Hara', 'Will', 'JPY', 'USD', 5000, JPYUSD))
    transactions.append(Transaction('Ben', 'Yaku', 'USD', 'JPY', 50, USDJPY))

    # allocate orders to at relevant nodes 
    for t in transactions:
        print t
        print "\n"
        source_node = netting_engine[t.src]
        order = NettingOrder('OUTGOING', t.from_account, t.src_amount)
        netting_engine[t.src].outgoing_orders.append(order)

        destination_node = netting_engine[t.dest]
        order = NettingOrder('INCOMING', t.to_account, t.dest_amount)
        destination_node.incoming_orders.append(order)

    # Perform netting operation at each node
    print "\n\n"
    print "----------------GBP----------------\n"
    netting_engine['GBP'].calculate_totals()
    print 'total incoming {0} GBP'.format(netting_engine['GBP'].total_incoming)
    print 'total outgoing {0} GBP\n'.format(netting_engine['GBP'].total_outgoing)
    netting_engine['GBP'].net_orders()
    netting_engine['GBP'].execute()
    print 'NET exposure {0} GBP'.format(netting_engine['GBP'].exposure)

    print "\n\n"

    print "----------------USD----------------\n"
    netting_engine['USD'].calculate_totals()
    print '\ntotal incoming {0} USD'.format((netting_engine['USD'].total_incoming))
    print 'total outgoing {0} USD\n'.format((netting_engine['USD'].total_outgoing))
    netting_engine['USD'].net_orders()
    netting_engine['USD'].execute()
    print 'NET exposure {0} USD'.format(netting_engine['USD'].exposure)

    print "\n\n"

    print "----------------JPY----------------\n"
    netting_engine['JPY'].calculate_totals()
    print 'total incoming {0} JPY'.format((netting_engine['JPY'].total_incoming))
    print 'total outgoing {0} JPY\n'.format((netting_engine['JPY'].total_outgoing))
    netting_engine['JPY'].net_orders()
    netting_engine['JPY'].execute()
    print 'NET exposure {0} JPY'.format(netting_engine['JPY'].exposure)


if __name__ == "__main__":
    main()
